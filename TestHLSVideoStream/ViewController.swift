//
//  ViewController.swift
//  TestHLSVideoStream
//
//  Created by Matt Croxson on 6/2/19.
//  Copyright © 2019 carsales.com Ltd. All rights reserved.
//

import AVKit
import UIKit

struct VideoPlaybackState {
    let seekTime: CMTime
    let isMuted: Bool

    static let defaultState = VideoPlaybackState(seekTime: CMTime.zero, isMuted: true)
    static let defaultUnmutedState = VideoPlaybackState(seekTime: CMTime.zero, isMuted: false)
}

struct ViewControllerModel {
    let autoPlay: Bool = true
    let aspectRatio: CGFloat = 1.7777
}

class ViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var statusLabel1: UILabel!
    @IBOutlet private weak var statusLabel2: UILabel!
    @IBOutlet private weak var statusLabel3: UILabel!

    // MARK - Properties

    private var avpController: AVPlayerViewController = AVPlayerViewController()
    private var itemReadyToPlayStatusObservation: NSKeyValueObservation?
    private let initialPlaybackState: VideoPlaybackState = VideoPlaybackState(seekTime: CMTime.zero, isMuted: true)


    private lazy var assetUrl1: URL = {
        guard let url = URL(string: "https://carsales.vxcrush.net/au/cars/private/1qfnv2qdmmkewqmyhhrey4y1y.mp4?vxc_format=hls") else {
            fatalError("URL 1 could not be decoded")
        }
        return url
    } ()

    private lazy var assetUrl2: URL = {
        guard let url = URL(string: "https://carsales.stream-cache.vxcrush.net/e91a6950946b3d53485eeacdc69260c6e0af38233ec98e7bfe8e1689962b75b0bdffdc412e0c1871dad8548c0c2bf4fd5ad0bfa51b01325131dd4e5ca91d458e_VXcrush_960x540_30Hz_2500bps.m3u8") else {

            fatalError("URL 2 could not be decoded")
        }
        return url
    } ()

    private lazy var assetUrl3: URL = {
        guard let url = URL(string: "https://scratch.vxcrush.net/ads/mm-mercedes-benz-x-class-6s/playback-master.m3u8") else {
            fatalError("URL 3 could not be decoded")
        }
        return url
    } ()

    private let assetLoadKeys: [String] = [
        "playable",
        "duration"
    ]

    private var needsInitialConfig: Bool = true

    private let model: ViewControllerModel = ViewControllerModel()

    // MARK - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.addText("UGV URL (API)\n\n\(assetUrl1)\n\n", to: statusLabel1, replaceContent: true)
        configureVideoPlayer(with: assetUrl1, label: statusLabel1)

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(15)) {
            self.reset()
            self.addText("UGV URL (Direct)\n\n\(self.assetUrl2)\n\n", to: self.statusLabel2, replaceContent: true)
            self.configureVideoPlayer(with: self.assetUrl2, label: self.statusLabel2)

            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(15)) {
                self.reset()
                self.addText("Video Ad URL\n\n\(self.assetUrl3)\n\n", to: self.statusLabel3, replaceContent: true)
                self.configureVideoPlayer(with: self.assetUrl3, label: self.statusLabel3)
            }
        }
    }

    private func reset() {
        itemReadyToPlayStatusObservation = nil
        containerView.subviews.forEach { subview in
            subview.removeFromSuperview()
        }
        avpController.player = nil
        avpController.removeFromParent()
        avpController = AVPlayerViewController()
        needsInitialConfig = true
    }

    private func configureVideoPlayer(with assetUrl: URL, label: UILabel) {
        addVideoPlayerController()

        let activityView = UIActivityIndicatorView(style: .whiteLarge)
        addActivityViewToVideoPlayer(activityView)

        let startTime = DispatchTime.now()

        self.addText("Starting activity indicator", to: label, since: startTime)
        activityView.startAnimating()

        self.addText("Configuring dispatch queue", to: label, since: startTime)

        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let `self` = self else { return }

            self.addText("Configuring AVPlayer with URL", to: label, since: startTime )
            let player = AVPlayer(url: assetUrl)

            self.addText("Loading values async from asset", to: label, since: startTime )
            player.currentItem?.asset.loadValuesAsynchronously(forKeys: self.assetLoadKeys) {
                DispatchQueue.main.async {
                    self.addText("Adding player to AVPVC", to: label, since: startTime )
                    self.avpController.player = player
                }

                self.addText("Adding status observation", to: label, since: startTime )
                self.itemReadyToPlayStatusObservation = player.currentItem?.observe(\.status) { item, _ in

                    DispatchQueue.main.async {

                        self.addText("Checking status is readyToPlay", to: label, since: startTime )
                        if item.status == .readyToPlay {

                            self.addText("Checking if needs initial config", to: label, since: startTime )
                            guard self.needsInitialConfig else { return }
                            self.needsInitialConfig = false

                            self.addText("Seeking -> \(self.initialPlaybackState.seekTime)", to: label, since: startTime )
                            player.seek(to: self.initialPlaybackState.seekTime)

                            self.addText("Muting -> \(self.initialPlaybackState.isMuted)", to: label, since: startTime )
                            player.isMuted = self.initialPlaybackState.isMuted

                            self.addText("Stopping activity indicator", to: label, since: startTime )
                            activityView.stopAnimating()

                            self.addText("Playing", to: label, since: startTime )
                            player.play()
                        }
                    }
                }
            }
        }
    }

    private func addVideoPlayerController() {
        addChildViewController(avpController, to: containerView)
        avpController.view.translatesAutoresizingMaskIntoConstraints = false
        avpController.allowsPictureInPicturePlayback = false
        let multiplier = model.aspectRatio > 0 ? 1 / model.aspectRatio : 1

        NSLayoutConstraint.activate([
            avpController.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            avpController.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            avpController.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            avpController.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            NSLayoutConstraint(item: avpController.view, attribute: .height, relatedBy: .equal, toItem: avpController.view, attribute: .width, multiplier: multiplier, constant: 0)
            ])
    }

    private func addActivityViewToVideoPlayer(_ activityView: UIActivityIndicatorView) {
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.hidesWhenStopped = true
        activityView.stopAnimating()
        avpController.contentOverlayView?.addSubview(activityView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: activityView, attribute: .centerX, relatedBy: .equal, toItem: avpController.contentOverlayView, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: activityView, attribute: .centerY, relatedBy: .equal, toItem: avpController.contentOverlayView, attribute: .centerY, multiplier: 1, constant: 100)
            ])
    }

    private func numberOfSeconds(since dispatchTime: DispatchTime) -> Double {
        let current = DispatchTime.now().uptimeNanoseconds

        return round((Double(current - dispatchTime.uptimeNanoseconds) / 1_000_000_000.0) * 1000) / 1000
    }

    private func addText(_ text: String, to label: UILabel, since startTime: DispatchTime? = nil, replaceContent: Bool = false) {
        var numberOfSecondsString = ""
        if let startTime = startTime {
            let numberOfSeconds = self.numberOfSeconds(since: startTime)
            numberOfSecondsString = "\(numberOfSeconds)"
        }
        DispatchQueue.main.async {
            if replaceContent {
                label.text = "\(numberOfSecondsString)\(numberOfSecondsString.isEmpty ? "" : ": ")\(text)"
            } else {
                label.text = "\(label.text!)\n\(numberOfSecondsString): \(text)"
            }
        }
    }


}


extension UIViewController {
    func addChildViewController(_ viewController: UIViewController,
                                to view: UIView,
                                leadingConstant: CGFloat = 0,
                                trailingConstant: CGFloat = 0,
                                topConstant: CGFloat = 0,
                                bottomConstant: CGFloat = 0) {
        addChild(viewController)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewController.view)
        NSLayoutConstraint.activate([
            viewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingConstant),
            viewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingConstant),
            viewController.view.topAnchor.constraint(equalTo: view.topAnchor, constant: topConstant),
            viewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomConstant)
            ])
        viewController.didMove(toParent: self)
    }
}
